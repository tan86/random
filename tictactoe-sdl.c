#include <SDL2/SDL.h>
// gcc -Wall -lSDL2 -o tic tictactoe-sdl.c

const int WIN_WIDTH   = 640;
const int WIN_HEIGHT  = 640;
const int CELL_WIDTH  = WIN_WIDTH / 3;
const int CELL_HEIGHT = WIN_HEIGHT / 3;
const SDL_Color GRID_COLOR     = { .r = 255, .g = 255, .b = 255 };
const SDL_Color PLAYER_X_COLOR = { .r = 255, .g = 50,  .b = 50 };
const SDL_Color PLAYER_O_COLOR = { .r = 50,  .g = 100, .b = 255 };
const SDL_Color TIE_COLOR      = { .r = 100, .g = 100, .b = 100 };

enum { RUNNING_STATE, PLAYER_X_WON, PLAYER_O_WON, TIE_STATE, QUIT_STATE };
enum { EMPTY, PLAYER_X, PLAYER_O };

typedef struct {
	int board[9];
	int player;
	int state;
} game_t;

game_t game;
SDL_Renderer* ren = {0};
SDL_Window* win   = {0};
SDL_Event ev      = {0};

void render_grid(const SDL_Color* col) {
	SDL_SetRenderDrawColor(ren, col->r, col->g, col->b, 255);
	for(int i = 1; i < 3; ++i) {
		SDL_RenderDrawLine(ren, i * CELL_WIDTH, 0, i * CELL_WIDTH, WIN_HEIGHT);
		SDL_RenderDrawLine(ren, 0, i * CELL_HEIGHT, WIN_WIDTH, i * CELL_HEIGHT);
	}
}

void render_cell(int r, int c, const SDL_Color* col) {
	SDL_Rect rect = {r*CELL_WIDTH+10, c*CELL_HEIGHT+10, CELL_WIDTH-20, CELL_HEIGHT-20};
	SDL_SetRenderDrawColor(ren, col->r, col->g, col->b, 255);
	SDL_RenderFillRect(ren, &rect);
}

void render_board(const SDL_Color* x_col, const SDL_Color* o_col, const SDL_Color* gcol) {
	render_grid(gcol);
	for(int i = 0; i < 3; ++i){
		for(int j = 0; j < 3; ++j){
			switch (game.board[i * 3 + j]) {
				case PLAYER_X:
					render_cell(i,j, x_col);
					break;
				case PLAYER_O:
					render_cell(i,j, o_col);
					break;
			}
		}
	}
}

void render_game() {
	switch(game.state) {
	case RUNNING_STATE:
		render_board(&PLAYER_X_COLOR, &PLAYER_O_COLOR, &GRID_COLOR);
		break;
	case PLAYER_X_WON:
		render_board(&PLAYER_X_COLOR, &PLAYER_X_COLOR, &PLAYER_X_COLOR);
		break;
	case PLAYER_O_WON:
		render_board(&PLAYER_O_COLOR, &PLAYER_O_COLOR, &PLAYER_O_COLOR);
		break;
	case TIE_STATE:
		render_board(&TIE_COLOR, &TIE_COLOR, &TIE_COLOR);
		break;
	}
}

int check_player_won(const int player) {
	int rc = 0;
	int cc = 0;
	int d1c = 0;
	int d2c = 0;

	for(int i = 0; i < 3; ++i){
		for(int j = 0; j < 3; ++j){
			if(game.board[i*3+j] == player) { ++rc; }
			if(game.board[j*3+i] == player) { ++cc; }
		}

		if(rc >= 3 || cc >= 3) { return 1;}
		rc=0; cc=0;

		if(game.board[i*3 + i] == player) { ++d1c; }
		if(game.board[(i*3) + 3 + i-1] == player) { ++d2c; }
	}

	return d1c >= 3 || d2c >= 3;
}

int empty_cells() {
	int c = 0;
	for(int i = 0; i < 9; ++i) {
		if(game.board[i] == EMPTY) ++c;
	}
	return c;
}

void game_over_conditions() {
	if(check_player_won(PLAYER_X)){
		game.state = PLAYER_X_WON;
	} else if(check_player_won(PLAYER_O)){
		game.state = PLAYER_O_WON;
	} else if(!empty_cells()){
		game.state = TIE_STATE;
	}
}

void click_on_cell(int row, int col) {
	if(game.state == RUNNING_STATE) {
		// turn
		if(game.board[row * 3 + col] == EMPTY) {
			game.board[row * 3 + col] = game.player;
			// switch
			game.player = (game.player == PLAYER_X) ? PLAYER_O : PLAYER_X;
			game_over_conditions();
		}
	} else {
    	game.player = PLAYER_X;
    	game.state = RUNNING_STATE;
    	for (int i = 0; i < 9; ++i) { game.board[i] = EMPTY; }
	}
}

int main(int arc, char** argv) {
	SDL_Init(SDL_INIT_VIDEO);
	win = SDL_CreateWindow("", 0, 0, WIN_WIDTH, WIN_HEIGHT, SDL_WINDOW_INPUT_FOCUS);
	ren = SDL_CreateRenderer(win, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);

	while(game.state != QUIT_STATE) {
		SDL_PollEvent(&ev);
		switch (ev.type) {
			case SDL_QUIT:
				game.state = QUIT_STATE;
				break;
			case SDL_MOUSEBUTTONDOWN:
				click_on_cell(ev.button.x/ CELL_HEIGHT, ev.button.y / CELL_WIDTH);
		}

		SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
		SDL_RenderClear(ren);
		render_game();
		SDL_RenderPresent(ren);
	}

	SDL_Quit();
	return 0;
}
